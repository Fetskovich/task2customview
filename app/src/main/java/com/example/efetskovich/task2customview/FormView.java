package com.example.efetskovich.task2customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.AttributedCharacterIterator;

/**
 * Created by e.fetskovich on 3/10/17.
 */

public class FormView extends LinearLayout implements View.OnClickListener {

    private TextView header;
    private final String NUMBER_REGEX = ".*[0-9].*";
    private EditText queryText;
    private TextView validationText;
    private Button closeValidationButton;

    public FormView(Context context) {
        super(context);
        init();
    }

    public FormView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        initAttributes(context, attrs);
    }

    public FormView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    public void onClick(View v) {
        hideValidationComponents();
    }

    private void init() {
        inflate(getContext(), R.layout.form_layout, this);
        initComponents();
        queryText.addTextChangedListener(textWatcher);
    }

    private void initComponents() {
        this.header = (TextView) findViewById(R.id.text_header);
        this.closeValidationButton = (Button) findViewById(R.id.close_validation);
        this.validationText = (TextView) findViewById(R.id.validation_text);
        this.queryText = (EditText) findViewById(R.id.query_text);
        this.closeValidationButton.setOnClickListener(this);
    }

    private void checkTextValidationOnNumbers(Editable s) {
        if (isThereAnyNumbersInText(s)) {
            this.validationText.setText(R.string.validation);
            showValidationComponents();
        } else {
            hideValidationComponents();
        }
    }


    private boolean isThereAnyNumbersInText(Editable s) {
        return s.toString().matches(NUMBER_REGEX) ? true : false;
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            checkTextValidationOnNumbers(s);
        }
    };


    private void showValidationComponents() {
        this.validationText.setVisibility(View.VISIBLE);
        this.closeValidationButton.setVisibility(View.VISIBLE);
    }

    private void hideValidationComponents() {
        this.validationText.setVisibility(View.INVISIBLE);
        this.closeValidationButton.setVisibility(View.INVISIBLE);
    }

    private void initAttributes(Context context, AttributeSet attrs){
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FormView,
                0, 0);
        try {
            float size = a.getDimension(R.styleable.FormView_validationTextSize, 20);
            validationText.setTextSize(size/getResources().getDisplayMetrics().density);
        } finally {
            a.recycle();
        }
    }


}
